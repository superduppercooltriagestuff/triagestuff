package com.example;
import java.io.Serializable;

public class Doctor implements Serializable {
    private String name;
    private String specialty;
    private boolean available;

    public Doctor(String name, String specialty){
        this.name = name;
        this.specialty = specialty;
        this.available = true;
    }

    public void treatPatient(Patient patient){
        // do stuff to patient
        available = false;
    }

    public void finish(){
        available = true;
    }

    public String getSpecialty() {
        return specialty;
    }

    public boolean isAvailable() {
        return available;
    }

    @Override
    public String toString(){
        return name;
    }
}