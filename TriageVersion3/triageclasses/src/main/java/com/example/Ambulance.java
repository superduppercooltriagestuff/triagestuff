package com.example;
import java.io.Serializable;
import java.util.ArrayList;

public class Ambulance implements Serializable {
    private static int numberAmbulances;
    private int ambulanceID;
    private ArrayList<Paramedic> paramedics;
    private Integer[] position;
    private Hospital destination;

    public Ambulance() {
        this.ambulanceID = numberAmbulances;
        numberAmbulances ++;
        this.paramedics = new ArrayList<>();
        this.position = new Integer[]{0,0};
        this.destination = null;
    }

    public void setPosition(Integer x, Integer y) {
        position = new Integer[]{x,y};
    }

    public void addParamedic(Paramedic paramedic) {
        paramedics.add(paramedic);
    }

    public Integer getDistance(Hospital hospital){
        int ans = (int)(Math.sqrt(Math.pow(position[0] - hospital.getLocation()[0], 2) +
                Math.pow(position[1] - hospital.getLocation()[1], 2)));
        return (Integer) ans;
    }

    public void setDestination(Hospital hospital){
        destination = hospital;
        /// find destination based on patient(s)
        // destination = hospital;
    }

    public void clearDestination(){
        destination = null;
    }

    public Hospital getDestination() {
        return destination;
    }

    @Override
    public String toString(){
        return "Ambulance #" + ambulanceID;
    }
}