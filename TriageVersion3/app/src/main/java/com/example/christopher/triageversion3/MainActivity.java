package com.example.christopher.triageversion3;

import android.content.Context;
import android.content.Intent;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class MainActivity extends AppCompatActivity {

    //TODO update static strings to better names
    public static String EXTRA_USER_ID = "com.example.christopher.traigeversion3.USERID";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    // Called upon clicking loginbutton --> creates new intent --> passes user + passcode
    // to the verification screen.
    public void submitLogin(View view){

        TextView responseBox = ((TextView) findViewById(R.id.responseBox));
        responseBox.setText("Authenticating");

        EditText usernameEditText = (EditText) findViewById(R.id.idText);
        EditText passwordEditText = (EditText) findViewById(R.id.passText);

        String loginresult = verifyLoginInformation(usernameEditText.getText().toString(),
                passwordEditText.getText().toString());

        // Resets username and passworld fields
        usernameEditText.setText("");
        passwordEditText.setText("");

        if (loginresult != null){
            // If verified, go onto main menu screen
            Intent intent = new Intent(this, MainMenuActivity.class);
            intent.putExtra(EXTRA_USER_ID, loginresult);
            startActivity(intent);
        }
        else{
            responseBox.setText("Incorrect ID/password. Please try again.");
        }
    }

    // Returns the paramedic ID if successfully authenticated, returns null if invalid credentials
    private String verifyLoginInformation(String username, String password) {
        // TODO authentication service


        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        String filename = "stuff.ser";
        FileInputStream fis = null;
        ObjectInputStream in = null;
        try {
            fis = new FileInputStream(filename);
            in = new ObjectInputStream(fis);
            TriageSystem myTriageSystem = (TriageSystem) in.readObject();
            //for (int i = 0; i < myTriageSystem.getUserNames().size(); i++) {
              //  if (myTriageSystem.getUserNames().get(i).get(0) == username &&
                //         myTriageSystem.getUserNames().get(i).get(1) == password) {
            for(String user: myTriageSystem.getUserNames().keySet()){
                if(user == username &&
                        myTriageSystem.getUserNames().get(username) == password){
                    in.close();
                    return "ID" + username;
                }
            }
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }
}