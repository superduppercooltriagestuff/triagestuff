import javafx.util.Pair;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;

public class Paramedic extends Ambulance implements Serializable {
    private String userID;
    private String password;
    private ArrayList<Patient> patients;

    public Paramedic(String userID, String password) {
        this.userID = userID;
        this.password = password;
        this.patients = new ArrayList<>();
    }

    public void createPatient(String sex, String age){
        Patient patient = new Patient(sex, age);
        patients.add(patient);
    }

    // maybe add things to catch errors
    public void setBloodPressure(int patientID, Pair<Integer, Integer> bloodPressure) {
        getPatient(patientID).setBloodPressure(bloodPressure);
    }

    public void setHeartRate(int patientID, double heartRate) {
        getPatient(patientID).setHeartRate(heartRate);
    }

    public void setPain(int patientID, String pain) {
        getPatient(patientID).setPain(pain);
    }

    public void setLOC(int patientID, String LOC) {
        getPatient(patientID).setLOC(LOC);
    }

    public Hospital sendPatientInfo(int patientID, TriageSystem myTriageSystem) {
        Patient patient = getPatient(patientID);
        ArrayList<Object> scores = new ArrayList<>();
        for(Hospital hospital: myTriageSystem.getHospitals()){
            ArrayList<Object> toAdd = new ArrayList<>(Arrays.asList(hospital,
                    (Integer) hospital.assessPatient(patient) + getDistance(hospital)));
            scores.add(toAdd);
        }
        return getMinimumValue(scores);
    }

    private Hospital getMinimumValue(ArrayList<Object> scores){
        Hospital ans = (Hospital) ((ArrayList<Object>) scores.get(0)).get(0);
        Integer minValue = (Integer) ((ArrayList<Object>) scores.get(0)).get(1);
        for (int i = 1; i < scores.size(); i++) {
            if((Integer) ((ArrayList<Object>) scores.get(i)).get(1) < minValue){
                minValue = (Integer) ((ArrayList<Object>) scores.get(i)).get(1);
                ans = (Hospital) ((ArrayList<Object>) scores.get(i)).get(0);
            }
        }
        return ans;
    }

    public void dispatchPatient(int patientID){
        patients.remove(getPatient(patientID));
    }

    public ArrayList<Patient> getPatients() {
        return patients;
    }

    public Patient getPatient(int patientID){
        for(int i = 0; i < patients.size(); i++) {
            if(patients.get(i).getPatientID() == patientID) {
                return patients.get(i);
            }
        }
        return null;
    }

    @Override
    public String toString(){
        return "Paramedic " + userID;
    }
}