package com.example;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;

public class Hospital implements Serializable {
    private String name;
    private Integer[] location;
    private TriageList<Patient> triageList;
    private ArrayList<Doctor> doctors;

    public Hospital(String name, Integer[] location){
        this.name = name;
        this.location = location;
        this.triageList = new TriageList();
        this.doctors = new ArrayList<>();
    }

    public void hireDoctor(String name, String specialty){
        Doctor doctor = new Doctor(name, specialty);
        doctors.add(doctor);
    }

    public Integer assessPatient(Patient patient){
        // find way to incorporate specialty scores
        int ans = 0;
        Iterator<Patient> iter = triageList.iterator();
        Patient current = triageList.peek();
        int i = 0;
        if(current == patient){
            ans = i;
        } else {
            while (iter.hasNext()) {
                current = iter.next();
                if (current == patient) {
                    ans = i;
                }
            }
        }
        triageList.remove(patient);
        return i;
    }

    public void addPatientToTriage(Patient patient){
        triageList.add(patient);
    }

    public String getName() {
        return name;
    }

    public Integer[] getLocation() {
        return location;
    }

    @Override
    public String toString(){
        return name;
    }
}