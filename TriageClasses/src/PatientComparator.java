import java.io.Serializable;
import java.util.Comparator;

public class PatientComparator implements Comparator<Patient>, Serializable {
    public int compare(Patient x, Patient y) {
        return x.getScore() - y.getScore();
    }
}
