package com.example.christopher.triageversion3;
import java.io.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TriageSystem implements Serializable {
    public static void main(String[] args){
        String filename = "stuff.ser";

        /// stuff doing
        System.out.println("Hello Patients!");
        TriageSystem myTriageSystem = new TriageSystem();
        Hospital generalHospital = new Hospital("General Hospital", new Integer[]{5,5});
        Hospital childrensHospital = new Hospital("Children's Hospital", new Integer[]{5,7});
        myTriageSystem.addHospital(generalHospital);
        myTriageSystem.addHospital(childrensHospital);
        generalHospital.hireDoctor("Dr. Who", "Trauma");
        generalHospital.hireDoctor("Dr. Strange", "Cardiac surgery");
        childrensHospital.hireDoctor("Dr. Seuss", "Pediatrics");
        myTriageSystem.createAmbulance();
        myTriageSystem.createAmbulance();
        Paramedic p1 = new Paramedic("Jeff", "bluetoes123", myTriageSystem);
        Paramedic p2 = new Paramedic("Rick", "lubadubdub", myTriageSystem);
        Paramedic p3 = new Paramedic("Morty", "geez123", myTriageSystem);
        myTriageSystem.getAmbulance(0).addParamedic(p1);
        myTriageSystem.getAmbulance(1).addParamedic(p2);
        myTriageSystem.getAmbulance(1).addParamedic(p3);
        p3.createPatient("M", "adult");
        System.out.println(p3.getPatients());
        p3.setHeartRate(0, 10);
        p3.getPatients().get(0).updateScore();
        System.out.println(p3.getPatients().get(0).getScore());
        myTriageSystem.getAmbulance(1).setDestination(p3.sendPatientInfo(0, myTriageSystem, myTriageSystem.getAmbulance(1)));
        System.out.println(myTriageSystem.getAmbulance(1).getDestination());
        p3.dispatchPatient(0);
        System.out.println(p3.getPatients());
        // done stuff doing

        // save the object to file
        FileOutputStream fos = null;
        ObjectOutputStream out = null;
        try {
            fos = new FileOutputStream(filename);
            out = new ObjectOutputStream(fos);
            out.writeObject(myTriageSystem);
            out.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        // read the object from file
        // save the object to file
        FileInputStream fis = null;
        ObjectInputStream in = null;
        try {
            fis = new FileInputStream(filename);
            in = new ObjectInputStream(fis);
            TriageSystem myTriageSystem2 = (TriageSystem) in.readObject();
            System.out.println(myTriageSystem2.getUserNames());
            //for (int i = 0; i < myTriageSystem2.getUserNames().size(); i++) {
              //  System.out.println(myTriageSystem2.getUserNames().get(i).get(0) +
                //        myTriageSystem2.getUserNames().get(i).get(1));
            for(String user: myTriageSystem2.getUserNames().keySet()) {
                System.out.println(user + myTriageSystem2.getUserNames().get(user));
            }
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private ArrayList<Hospital> hospitals;
    private ArrayList<Ambulance> ambulances;
    private Map<String, String> userNames;
    transient private Thread myThread;

    public TriageSystem() {
        this.hospitals = new ArrayList<>();
        this.ambulances = new ArrayList<>();
        this.myThread = new Thread();
        this.userNames = new HashMap<>();
    }

    public boolean addHospital(Hospital hospital){
        if(!hospitals.contains(hospital)) {
            hospitals.add(hospital);
            return true;
        } else {
            return false;
        }
    }

    public void createAmbulance(){
        Ambulance ambulance = new Ambulance();
        ambulances.add(ambulance);
        System.out.println("New ambulance: " + ambulance.toString());
    }

    public Ambulance getAmbulance(int ambulance) {
        return ambulances.get(ambulance);
    }

    public ArrayList<Hospital> getHospitals() {
        return hospitals;
    }

    public void addUser(String userName, String password){
        //ArrayList<String> toAdd = new ArrayList<>();
        //toAdd.add(userName);
        //toAdd.add(password);
        //System.out.println(toAdd);
        //userNames.add(new ArrayList<String>(2));
        //System.out.println(userNames);
        //userNames.get(userNames.size() - 1).add(userName);
        //userNames.get(userNames.size() - 1).add(password);
        userNames.put(userName, password);
        System.out.println(userNames);
    }

    public Map<String, String> getUserNames() {
        return userNames;
    }
}