package com.example.christopher.triageversion3;
import java.io.Serializable;
import java.util.Comparator;
import java.util.PriorityQueue;

public class TriageList<Patient> extends PriorityQueue<Patient> implements Serializable {
    public TriageList() {
        super(500, (Comparator<? super Patient>) new PatientComparator());
    }
}