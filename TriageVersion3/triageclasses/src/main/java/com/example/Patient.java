package com.example;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Patient implements Serializable {
    private static int numberPatients = 0;
    private int patientID;
    private String sex;    // "M" or "F"
    private String age;    // "infant", "child" or "adult"
    private Integer[] bloodPressure;   /// (Systolic, Diastolic)
    private double heartRate;
    private String pain; // choose from options?

    private String LOC; // AVPU
    private static final Map<String, Integer> levels;
    static {
        levels = new HashMap<>();
        levels.put("A", 0);
        levels.put("V", 1);
        levels.put("P", 2);
        levels.put("U", 3);
    }

    private ArrayList<String> allergies;
    private ArrayList<String> medication;

    private int score; // score for triage (initially 0)

    public Patient(String sex, String age) {
        this.patientID = numberPatients;
        numberPatients ++;
        this.sex = sex;
        this.age = age;
        this.pain = null;
        this.LOC = "A";
        this.allergies = new ArrayList<>();
        this.medication = new ArrayList<>();
        /// 6 arrangements
        if (age == "infant") {
            this.bloodPressure = new Integer[]{45,70};
            this.heartRate = 135;
        }
        if (age == "child") {
            this.bloodPressure = new Integer[]{60, 90};
            this.heartRate = 90;
        } else {
            if(sex == "M"){
                this.bloodPressure = new Integer[]{75, 130};
                this.heartRate = 80;
            } else {
                this.bloodPressure = new Integer[]{75, 120};
                this.heartRate = 80;
            }
        }
    }

    public void updateScore(){
        int ans = 0;

        // LOC (AVPU)
        ans += levels.get(LOC);

        if (age == "infant") {
            if (bloodPressure[0] < 30 | bloodPressure[0] > 60 |
                    bloodPressure[1] < 65 | bloodPressure[1] > 90) {
                ans += 2;
            }
            if (heartRate < 120 | heartRate > 150) {
                ans += 2;
            }
        }
        if (age == "child") {
            if(bloodPressure[0] < 55 | bloodPressure[0] > 65 |
                    bloodPressure[1] < 80 | bloodPressure[1] > 100)  {
                ans += 2;
            }
            if(heartRate < 75 | heartRate > 105){
                ans += 2;
            }
        } else {
            if(sex == "M"){
                if(bloodPressure[0] < 60 | bloodPressure[0] > 90 |
                        bloodPressure[1] < 115 | bloodPressure[1] > 145){
                    ans += 2;
                }
                if(heartRate < 60 | heartRate > 100){
                    ans += 2;
                }
            } else {
                if(bloodPressure[0] < 60 | bloodPressure[0] > 90 |
                        bloodPressure[1] < 105 | bloodPressure[1] > 135){
                    ans += 2;
                }
                if(heartRate < 60 | heartRate > 100){
                    ans += 2;
                }
            }
        }
        score = ans;
    }

    public String getSex() {
        return sex;
    }

    public String getAge() {
        return age;
    }

    public Integer[] getBloodPressure() {
        return bloodPressure;
    }

    public double getHeartRate() {
        return heartRate;
    }

    public String getPain() {
        return pain;
    }

    public String getLOC() {
        return LOC;
    }

    public ArrayList<String> getAllergies() {
        return allergies;
    }

    public ArrayList<String> getMedication() {
        return medication;
    }

    public int getScore() {
        return score;
    }

    public int getPatientID(){
        return patientID;
    }

    public void setBloodPressure(Integer[] bloodPressure) {
        this.bloodPressure = bloodPressure;
    }

    public void setHeartRate(double heartRate) {
        this.heartRate = heartRate;
    }

    public void setPain(String pain) {
        this.pain = pain;
    }

    public void setLOC(String LOC) {
        this.LOC = LOC;
    }

    @Override
    public boolean equals(Object other){
        if(other instanceof Patient){
            Patient otherPatient = (Patient) other;
            return patientID == otherPatient.getPatientID();
        } else {
            return false;
        }
    }

    @Override
    public String toString(){
        return "Patient #" + patientID;
    }
}